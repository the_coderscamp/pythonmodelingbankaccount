 #################################
 # Created by Sum Abiut on 16/03/19.
 #################################

class Bankaccount:
    """docstring fo Bankaccount."""

#our constructor
    def __init__(self,accountNumber,balance,customerName,email,phoneNumber):
        self.accountNumber=accountNumber
        self.balance=balance
        self.customerName=customerName
        self.email=email
        self.phoneNumber=phoneNumber


    def getaccountNumber(self):
            return self.accountNumber

    def setaccountNumber(self,accountNumber):
            self.accountNumber=accountNumber

    def getCustomerName(self):
        return self.getCustomerName

    def setCustomerName(self,customerName):
        self.customerName=customerName

    def getBalance(self, balance):
        self.balance=balance;

#customer making a depost
    def depost(self,depositAmount):
        self.balance+=depositAmount
        print('You have deposted',format(depositAmount,',d'), 'VT,''to the accout', self.accountNumber)
        print('Your new balnce is: ', format(self.balance,',d'),'VT',)
        print('\n')

#customer withdrawing money fromm his account
    def withdraw(self,withDrawAmount):
        if(self.balance-withDrawAmount<0):
            print('The withdraw Amount is more than the amount of money you have in ',self.accountNumber, 'Account')
        else:
            self.balance=self.balance-withDrawAmount
            print('You have withdraw ', format(withDrawAmount,',d'),'VT','from your accout','Your current balance is now',format(self.balance, ',d'),'VT')

        #displaying the customer details
    def displayCustomer(self):
            print('Customer Details:')
            print('=========================== \n')
            print('customer Name:', self.customerName)
            print('Phone:', self.phoneNumber)
            print('Account Number:', self.accountNumber)
            print('Current Banlance:', self.balance, 'VT')
            print('\n')
            print('=========================== \n')

#Creating a new object of Bank account class
def main():
    customer=Bankaccount(123654,5000,'Jame Ken','jken@gmail.com', 7596847)
    customer2=Bankaccount(136547,8000,'John Ben','jb@gmail.com', 7596697)

    customer.displayCustomer()
    customer2.displayCustomer()
#uncomment this lines below to test
#you can change the deposit amount or withdraw and set new customer names
    #customer.depost(8000)
    #customer.withdraw(7000)
    #customer.setCustomerName()

    #customer2.depost(5000)
    #customer2.withdraw(4000)
    #customer2.setCustomerName()


#Start the program, calling the main function
main()
